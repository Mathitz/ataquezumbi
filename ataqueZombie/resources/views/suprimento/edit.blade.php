@extends('layout')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Faça edição dos seus itens:
    </div>
<div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        {{Form::open(['route' => ['suprimento.update', $suprimento->id]])}}
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="descricao">Descrição:</label>
                <input type="text" class="form-control" name="descricao" value={{ $suprimento->descricao }} />
            </div>
            <div class="form-group">
                <label for="qtde">Quantidade:</label>
                <input type="number" class="form-control" name="qtde" value={{ $suprimento->qtde }} />
            </div>
            <div class="form-group">
                <label for="obs">Observação:</label>
                <input type="text" class="form-control" name="obs" value={{ $suprimento->obs }} />
            </div>
            <div class="form-group">
                <label for="name_up">Usuário: {{ Auth::user()->name }} </label>
                <input type="hidden" class="form-control" name="name" value="{{$suprimento->name}}"/>
                <input type="hidden" class="form-control" name="name_up" value="{{Auth::user()->name}}"/>
            </div>
            {{Form::submit('Atualizar', ['class' => 'btn btn-primary'])}}
        {{Form::close()}}
    </div>
</div>
@endsection