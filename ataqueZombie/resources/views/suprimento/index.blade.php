@extends('layout')

@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Kit de Sobrevivência
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br />
    @endif
    <table class="table table-striped bg">
        <thead>
            <tr class="title">
                <td>ID</td>
                <td>Descrição</td>
                <td>Quantidade</td>
                <td>Observação</td>
                <td>Criado em</td>
                <td>Atualizado em</td>
                <td colspan="2">Action</td>
                <td><a href="{{ route('suprimento.create') }}" class="btn btn-success">Criar novo item</a></td>
            </tr>
        </thead>
        <tbody>
            @foreach($suprimento as $suprimento)
            <tr class="bold">
                <td>{{$suprimento->id}}</td>
                <td>{{$suprimento->descricao}}</td>
                <td>{{$suprimento->qtde}}</td>
                <td>{{$suprimento->obs}}</td>
                <td>{{$suprimento->created_at}} por {{ $suprimento->name }}</td>
                <td>{{$suprimento->updated_at}} por {{ $suprimento->name_up }}</td>
                <td><a href="{{ route('suprimento.edit',$suprimento->id)}}" class="btn btn-primary">Edit</a></td>
                <td>
                    {{Form::open(['route' => ['suprimento.destroy', $suprimento->id]])}}
                        @csrf
                        @method('DELETE')
                        {{Form::submit('Deletar', ['class' => 'btn btn-danger'])}}
                    {{Form::close()}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection