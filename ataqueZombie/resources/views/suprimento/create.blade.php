@extends('layout')

@section('content')
<style>
    .uper {
    margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Adicione um item a seu kit de sobrevivência:
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br />
        @endif
            {{Form::open(['route' => 'suprimento.store'])}}
                <div class="form-group">
                    @csrf
                    <label for="descricao">Descrição:</label>
                    <input type="textarea" class="form-control" name="descricao"/>
                </div>
                <div class="form-group">
                    <label for="qtde">Quantidade:</label>
                    <input type="number" class="form-control" name="qtde"/>
                </div>
                <div class="form-group">
                    <label for="obs">Observação:</label>
                    <input type="text" class="form-control" name="obs"/>
                </div>
                <div class="form-group">
                    <label for="name">Usuário: {{ Auth::user()->name }} </label>
                    <input type="hidden" class="form-control" name="name" value="{{Auth::user()->name}}"/>
                    <input type="hidden" class="form-control" name="name_up" value="{{Auth::user()->name}}"/>
                </div>
                {{Form::submit('Adicionar', ['class' => 'btn btn-primary'])}}
            {{Form::close()}}
    </div>
</div>
@endsection