<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuprimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suprimentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao', 255);
            $table->integer('qtde');
            $table->string('obs', 100);
            $table->string('name')->default('');
            $table->string('name_up')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suprimentos');
    }
}
