<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suprimento extends Model
{
    protected $fillable = [
        'descricao',
        'qtde',
        'obs',
        'name',
        'name_up',
    ];
}
