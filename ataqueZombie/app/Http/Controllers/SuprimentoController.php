<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suprimento;
use Illuminate\Support\Facades\Auth;

class SuprimentoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suprimento = Suprimento::all();

        return view('suprimento.index', compact('suprimento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suprimento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate
        ([
            'descricao'=>'required',
            'qtde'=> 'required|integer',
            'obs' => 'required',
            'name'=> 'required',
            'name_up'=> 'required',
        ]);

        $suprimento = new Suprimento
        ([
            'descricao' => $request->   get('descricao'),
            'qtde'=> $request->         get('qtde'),
            'obs'=> $request->          get('obs'),
            'name'=> $request->         get('name'),
            'name_up'=> $request->      get('name_up'),
        ]);

        $suprimento->save();
        return redirect('/suprimento')
        ->with('success', 'Seu item foi adicionado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suprimento = Suprimento::find($id);

        return view('suprimento.edit', compact('suprimento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate
        ([
            'descricao'=>   'required',
            'qtde'=>        'required|integer',
            'obs' =>        'required',
            'name'=> 'required',
            'name_up'=> 'required',

        ]);

        $suprimento = Suprimento::find($id);
        $suprimento->descricao = $request->     get('descricao');
        $suprimento->qtde = $request->          get('qtde');
        $suprimento->obs = $request->           get('obs');
        $suprimento->name = $request->          get('name');
        $suprimento->name_up = $request->          get('name_up');
        $suprimento->save();

        return redirect('/suprimento')
        ->with('success', 'Seu kit foi atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suprimento = Suprimento::find($id);
        $suprimento->delete();

        return redirect('/suprimento')->with('success', 'Este item do seu kit foi removido!');
    }
}
